<?php


namespace App\Model;


class User
{
    public int    $id            = 0;
    public string $username      = '';
    public string $displayName   = '';
    public float  $balance       = 0;
    public string $plainPassword = '';
    public string $passwordHash  = '';
    public string $salt          = '';
}