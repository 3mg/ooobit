<?php


namespace App;


use App\Controller\BaseController;
use App\Controller\ExceptionController;
use App\Exception\FrameworkException;
use App\Exception\UnauthenticatedException;
use App\Service\Container;
use App\Service\Security;
use App\Service\SessionHelper;
use App\Service\UserManager;
use Exception;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use PDO;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Throwable;

class App
{
    protected Container       $container;
    protected LoggerInterface $logger;
    protected string          $env;
    protected array           $config;
    protected array           $controllers = [];

    public function __construct($env)
    {
        $this->env = $env;

        $config = require dirname(__DIR__).'/config/config.php';
        $configEnvPath = dirname(__DIR__).'/config/config_'.$env.'.php';

        if (file_exists($configEnvPath)) {
            $config = array_merge($config, require $configEnvPath);
        }

        $this->config = array_merge(
            [
                'debug'                   => false,
                'db_host'                 => '',
                'db_port'                 => null,
                'db_user'                 => '',
                'db_password'             => '',
                'db_name'                 => '',
                'db_driver_options'       => [],
                'logs_dir'                => dirname(__DIR__).'/var/logs/',
                'logs_level'              => 'error',
                'session_cookie_lifetime' => 0,
                'routes'                  => [],
                'login_url'               => '/',
            ],
            $config
        );

        if (php_sapi_name() === 'cli') {
            ini_set('log_errors', $this->config['debug'] ? 1 : 0);
            ini_set('display_errors', 0);
        } else {
            ini_set('display_errors', $this->config['debug'] ? 1 : 0);
        }

        $this->logger = new Logger('app');
        $this->logger->pushHandler(
            new StreamHandler(
                sprintf('%s/%s.log', $this->config['logs_dir'], $env),
                Logger::toMonologLevel($this->config['logs_level'])
            )
        );
    }

    public function start()
    {
        $this->setErrorHandler();

        try {
            $this->initContainer();
        } catch (Exception $e) {
            throw new FrameworkException('Failed to build container', 0, $e);
        }

        $this->logger->info('App started');
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws Throwable
     */
    public function handleRequest(ServerRequestInterface $request): ResponseInterface
    {
        $this->logger->info('Start request handling');

        try {
            $response = call_user_func($this->resolveRoute($request));
        } catch (UnauthenticatedException $e) {
            $controller = $this->getController(ExceptionController::class, $request);
            $response = call_user_func([$controller, 'redirect'], $this->config['login_url']);
        } catch (Throwable $e) {
            $this->logger->error($e->getMessage(), ['exception' => $e]);

            if ($this->config['debug']) {
                throw $e;
            }

            $response = call_user_func($this->resolveException($request));
        }

        $this->logger->info('Finish request handling');

        return $response;
    }

    public function terminate(): void
    {
        $this->logger->info('App terminated');
    }

    /**
     * @return Container
     */
    public function getContainer(): Container
    {
        return $this->container;
    }

    private function setErrorHandler(): void
    {
        set_error_handler(
            function ($errno, $errstr, $errfile, $errline) {
                $this->logger->critical('Unhandled error', [$errno, $errstr, $errfile, $errline]);

                if (!$this->config['debug']) {
                    if (php_sapi_name() === 'cli') {
                        echo 'Unexpected error'.PHP_EOL;
                    } else {
                        require __DIR__.'/Resources/views/500.html.php';
                    }
                    exit;
                }

                return false;
            }
        );

        set_exception_handler(
            function (Throwable $exception) {
                $this->logger->critical(
                    'Unhandled exception: '.$exception->getMessage(),
                    [
                        'exception' => $exception,
                        'file'      => $exception->getFile(),
                        'line'      => $exception->getLine(),
                        'trace'     => $exception->getTraceAsString(),
                    ]
                );

                if (!$this->config['debug']) {
                    if (php_sapi_name() === 'cli') {
                        echo 'Unexpected error'.PHP_EOL;
                    } else {
                        require __DIR__.'/Resources/views/500.html.php';
                    }
                    exit;
                }

                throw $exception;
            }
        );
    }

    private function initContainer(): void
    {
        $this->container = new Container();

        $pdo = $this->initDB();
        $userManager = new UserManager($pdo, $this->logger);
        $sessionHelper = new SessionHelper($this->config['session_cookie_lifetime']);

        $this->container->build(
            $this->config,
            [
                Security::class        => new Security($userManager, $sessionHelper),
                UserManager::class     => $userManager,
                PDO::class             => $pdo,
                LoggerInterface::class => $this->logger,
                SessionHelper::class   => $sessionHelper,
            ]
        );
    }

    private function initDB(): PDO
    {
        $dsn = "mysql:host={$this->config['db_host']};port={$this->config['db_port']};dbname={$this->config['db_name']};";

        return new PDO(
            $dsn, $this->config['db_user'], $this->config['db_password'], [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            ]
        );
    }

    private function resolveRoute(ServerRequestInterface $request): callable
    {
        $parsedUrl = parse_url($request->getUri());

        if (!$parsedUrl) {
            return $this->resolveNotFound($request);
        }

        foreach ($this->config['routes'] as $path => $route) {
            if (
                $path === $parsedUrl['path']
                && (strtoupper($route['method']) === 'ANY' || strtoupper($route['method']) === strtoupper(
                        $request->getMethod()
                    ))
            ) {
                $controller = $this->getController($route['controller'], $request);
                $action = [$controller, $route['action'].'Action'];

                if (is_callable($action)) {
                    return $action;
                }
            }
        }

        return $this->resolveNotFound($request);
    }

    private function resolveNotFound(ServerRequestInterface $request): callable
    {
        $controller = $this->getController(ExceptionController::class, $request);

        return [$controller, 'notFoundAction'];
    }

    private function getController(string $className, ServerRequestInterface $request): BaseController
    {
        if (!array_key_exists($className, $this->controllers)) {
            $this->controllers[$className] = new $className($this->container, $request);
        }

        return $this->controllers[$className];
    }

    private function resolveException(ServerRequestInterface $request, Throwable $exception): callable
    {
        /** @var ExceptionController $controller */
        $controller = $this->getController(ExceptionController::class, $request);
        $controller->setException($exception);

        return [$controller, 'exceptionAction'];
    }
}