<?php
$title = '404';
$content = <<<EOF
Page not found. 
EOF;
?>

<?php include 'includes/start.html.php'; ?>
    <h1><?= $title; ?></h1>
    <p><?= $content; ?></p>
<?php include 'includes/end.html.php'; ?>