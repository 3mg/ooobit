<?php include dirname(__DIR__).'/includes/start.html.php'; ?>
    <form method="post" action="/withdraw">

        <?php if ($error): ?>
            <q>
                <mark><?= strip_tags($error) ?></mark>
            </q>
        <?php endif; ?>

        <fieldset>
            <legend>Withdraw</legend>

            <label for="sum">Sum</label>
            <input type="number" name="sum" id="sum">

            <input name="submit" type="submit">
        </fieldset>

    </form>

    <p><a href="/profile">Profile</a></p>
    <p><a href="/">Main page</a></p>
    <p><a href="/logout">Logout</a></p>
<?php include dirname(__DIR__).'/includes/end.html.php'; ?>