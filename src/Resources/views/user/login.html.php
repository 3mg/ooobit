<?php include dirname(__DIR__).'/includes/start.html.php'; ?>
    <form method="post" action="/login">

        <?php if ($error): ?>
            <q>
                <mark><?= $error ?></mark>
            </q>
        <?php endif; ?>

        <fieldset>
            <legend>Login</legend>

            <label for="username">Username</label>
            <input type="text" name="username" id="username" placeholder="Username">

            <label for="password">Password</label>
            <input type="password" name="password" id="password" placeholder="password">

            <input type="hidden" name="token" id="token" value="<?= $token ?>">

            <input name="submit" type="submit">
        </fieldset>

    </form>

    <p><a href="/">Main page</a></p>
<?php include dirname(__DIR__).'/includes/end.html.php'; ?>