<?php include dirname(__DIR__).'/includes/start.html.php'; ?>
    <h1>Hello, <?= strip_tags($user->username) ?> </h1>
    <p>Your balance is: <?= $user->balance ?> </p>

    <p><a href="/withdraw">Withdraw</a></p>
    <p><a href="/">Main page</a></p>
    <p><a href="/logout">Logout</a></p>
<?php include dirname(__DIR__).'/includes/end.html.php'; ?>