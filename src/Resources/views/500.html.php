<?php
$title = 'Error';
$content = <<<EOF
Unexpected error. Please try again later. 
EOF;
?>

<?php include 'includes/start.html.php'; ?>
    <h1><?= $title; ?></h1>
    <p><?= $content; ?></p>
<?php include 'includes/end.html.php'; ?>