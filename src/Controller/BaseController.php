<?php


namespace App\Controller;


use App\Exception\UnauthenticatedException;
use App\Service\Container;
use App\Service\Security;
use App\Service\SessionHelper;
use Jasny\HttpMessage\Response;
use Jasny\HttpMessage\Stream;
use LogicException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;

abstract class BaseController
{
    protected Container              $container;
    protected ServerRequestInterface $request;

    public function __construct(Container $container, ServerRequestInterface $request)
    {
        $this->container = $container;
        $this->request = $request;
    }

    public function redirect($url, int $code = 302): ResponseInterface
    {
        return ($this->createResponse(sprintf('Redirecting to: %s', $url), $code))
            ->withHeader('Location', $url);
    }

    protected function createResponse($data, int $code = 200, string $contentType = 'text/html'): ResponseInterface
    {
        if (!$data instanceof StreamInterface) {
            $body = new Stream();
            $body->write($data);
        } else {
            $body = $data;
        }

        return (new Response())
            ->withStatus($code)
            ->withHeader('Content-Type', $contentType)
            ->withBody($body);
    }

    /**
     * @throws UnauthenticatedException
     */
    protected function requireAuthentication(): void
    {
        if (!$this->container->get(Security::class)->getAuthenticatedUser()) {
            throw new UnauthenticatedException();
        }
    }

    /**
     * @param string $template
     * @param array  $vars
     * @return string
     */
    protected function renderTemplate(string $template, array $vars = []): string
    {
        $path = dirname(__DIR__).'/Resources/views/'.$template;

        if (!file_exists($path)) {
            throw new LogicException(sprintf('Template %s not found', $path));
        }

        /** @var Security $security */
        $security = $this->container->get(Security::class);
        $vars['username'] = $security->getAuthenticatedUserName();

        ob_start();
        extract($vars);
        require $path;
        $out = ob_get_contents();
        ob_end_clean();

        return $out;
    }

    protected function getCSRFToken(): ?string
    {
        /** @var SessionHelper $sessionHelper */
        $sessionHelper = $this->container->get(SessionHelper::class);

        $sessionHelper->startSession();
        $token = $_SESSION['token'] ?? null;
        $_SESSION['token'] = bin2hex(random_bytes(32));
        $sessionHelper->writeAndClose();

        return $token;
    }
}