<?php


namespace App\Controller;


use Psr\Http\Message\ResponseInterface;
use Throwable;

class ExceptionController extends BaseController
{
    protected ?Throwable $exception;

    public function notFoundAction(): ResponseInterface
    {
        return $this->createResponse($this->renderTemplate('404.html.php'), 404);
    }

    public function exceptionAction(): ResponseInterface
    {
        return $this->createResponse($this->renderTemplate('500.html.php'), 500);
    }

    public function setException(?Throwable $exception): ExceptionController
    {
        $this->exception = $exception;

        return $this;
    }
}