<?php


namespace App\Controller;


use App\Exception\InvalidCredentialsException;
use App\Service\Security;
use App\Service\UserManager;
use PDO;
use Psr\Http\Message\ResponseInterface;
use Throwable;

class UserController extends BaseController
{
    public function profileAction(): ResponseInterface
    {
        $this->requireAuthentication();

        /** @var Security $security */
        $security = $this->container->get(Security::class);

        return $this->createResponse(
            $this->renderTemplate('user/profile.html.php', ['user' => $security->getAuthenticatedUser()])
        );
    }

    public function loginAction(): ResponseInterface
    {
        /** @var Security $security */
        $security = $this->container->get(Security::class);

        if ($security->getAuthenticatedUserName()) {
            return $this->redirect('/profile');
        }

        $prevToken = $this->getCSRFToken();
        $token = $_SESSION['token'];

        if ($this->request->getMethod() === 'POST') {
            $data = $this->request->getParsedBody();

            $renderFormError = function ($error) use ($token) {
                return $this->createResponse(
                    $this->renderTemplate('user/login.html.php', ['error' => $error, 'token' => $token]),
                    400
                );
            };

            foreach (['token', 'username', 'password', 'submit'] as $field) {
                if (!array_key_exists($field, $data)) {
                    return $renderFormError('Invalid form');
                }
            }

            if ($data['token'] !== $prevToken) {
                return $renderFormError('Invalid CSRF token');
            }

            try {
                $security->authenticateUser($data['username'], $data['password']);
            } catch (InvalidCredentialsException $e) {
                return $renderFormError('Invalid username or password');
            }

            return $this->redirect('/profile');
        }

        return $this->createResponse(
            $this->renderTemplate('user/login.html.php', ['error' => null, 'token' => $token])
        );
    }

    public function logoutAction(): ResponseInterface
    {
        /** @var Security $security */
        $security = $this->container->get(Security::class);
        $security->logoutUser();

        return $this->redirect('/');
    }

    public function withdrawAction(): ResponseInterface
    {
        $this->requireAuthentication();

        if ($this->request->getMethod() === 'POST') {
            $data = $this->request->getParsedBody();

            $renderFormError = function ($error) {
                return $this->createResponse($this->renderTemplate('user/withdraw.html.php', ['error' => $error]), 400);
            };

            foreach (['submit', 'sum'] as $field) {
                if (!array_key_exists($field, $data)) {
                    return $renderFormError('Invalid form');
                }
            }

            /** @var Security $security */
            $security = $this->container->get(Security::class);
            /** @var UserManager $userManager */
            $userManager = $this->container->get(UserManager::class);
            /** @var PDO $pdo */
            $pdo = $this->container->get(PDO::class);

            $pdo->beginTransaction();

            try {
                $user = $userManager->findUserByUsername($security->getAuthenticatedUserName(), true);
                $sum = (float)$data['sum'];

                if ($sum <= 0) {
                    $pdo->rollBack();

                    return $renderFormError('Invalid sum');
                }

                if ($sum > $user->balance) {
                    $pdo->rollBack();

                    return $renderFormError('Not enough balance');
                }

                $user->balance -= $sum;
                $userManager->saveUser($user);

                $pdo->commit();
            } catch (Throwable $e) {
                $pdo->rollBack();

                throw $e;
            }

            return $this->redirect('/profile');
        }

        return $this->createResponse($this->renderTemplate('user/withdraw.html.php', ['error' => null]));
    }
}