<?php


namespace App\Controller;


use App\Service\Security;
use Psr\Http\Message\ResponseInterface;

class IndexController extends BaseController
{
    public function indexAction(): ResponseInterface
    {
        /** @var Security $security */
        $security = $this->container->get(Security::class);

        return $this->createResponse(
            $this->renderTemplate('index.html.php', ['user' => $security->getAuthenticatedUser()])
        );
    }
}