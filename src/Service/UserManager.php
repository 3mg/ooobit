<?php


namespace App\Service;


use App\Model\User;
use LogicException;
use PDO;
use Psr\Log\LoggerInterface;

class UserManager
{
    private const FIELDS = ['username', 'salt', 'passwordHash', 'displayName', 'balance'];
    private PDO             $pdo;
    private LoggerInterface $logger;
    private string          $selectFields;
    private string          $updateFields;

    public function __construct(PDO $pdo, LoggerInterface $logger)
    {
        $this->pdo = $pdo;
        $this->logger = $logger;
        $this->selectFields = implode(',', array_merge(['id'], self::FIELDS));
        $this->updateFields = $this->mapSQLFieldSets(self::FIELDS);
    }

    public function findUserById(int $id, bool $selectForUpdate = false): ?User
    {
        return $this->findByCondition(['id' => $id], $selectForUpdate);
    }

    public function findUserByUsername(string $login, bool $selectForUpdate = false): ?User
    {
        return $this->findByCondition(['username' => $login], $selectForUpdate);
    }

    public function saveUser(User $user): void
    {
        if ($user->plainPassword !== '') {
            $hash = password_hash($user->plainPassword, PASSWORD_DEFAULT);

            if ($hash) {
                $user->passwordHash = $hash;
            }

            $user->plainPassword = '';
        }

        if ($user->id) {
            $sql = sprintf('UPDATE users SET %s WHERE id = :id', $this->updateFields);
            $initParams = ['id' => $user->id];
        } else {
            $sql = sprintf(
                'INSERT INTO users (%s) VALUES (%s)',
                implode(',', self::FIELDS),
                implode(',', array_map(fn($f) => ':'.$f, self::FIELDS))
            );
            $initParams = [];
        }

        $params = array_reduce(
            self::FIELDS,
            function ($result, $field) use ($user) {
                $result[$field] = $user->$field;

                return $result;
            },
            $initParams
        );
        $this->logger->debug('Executing SQL query: '.$sql, ['params' => $params]);

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);

        if (!$user->id) {
            $user->id = $this->pdo->lastInsertId();
        }
    }

    /**
     * Gets array of fields ['field1', 'field2', ...]
     * Returns string formatted as 'field1 = :field1, field2 = :field2, ...'
     *
     * @param array $fields
     * @return string
     */
    private function mapSQLFieldSets(array $fields): string
    {
        return implode(',', array_map(fn($f) => "{$f} = :{$f}", $fields));
    }

    private function findByCondition(array $condition, bool $selectForUpdate = false): ?User
    {
        if ($selectForUpdate && !$this->pdo->inTransaction()) {
            throw new LogicException('SELECT FOR UPDATE must be used inside transaction');
        }

        $conditionSting = $this->mapSQLFieldSets(array_keys($condition));

        $sql = sprintf(
            'SELECT %s FROM users WHERE %s %s',
            $this->selectFields,
            $conditionSting,
            $selectForUpdate ? 'FOR UPDATE' : ''
        );
        $this->logger->debug('Executing SQL query: '.$sql, ['params' => $condition]);

        $stmt = $this->pdo->prepare($sql);
        $stmt->setFetchMode(PDO::FETCH_CLASS, User::class);

        $stmt->execute($condition);

        return $stmt->fetch() ?: null;
    }
}