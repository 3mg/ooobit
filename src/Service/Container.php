<?php


namespace App\Service;


use LogicException;

class Container
{
    private array $parameters = [];
    private array $services   = [];

    private bool $isBuilt = false;

    /**
     * Container constructor.
     * @param array $parameters
     * @param array $services
     */
    public function build(array $parameters, array $services): void
    {
        if ($this->isBuilt) {
            throw new LogicException("Trying to build already built container");
        }

        $this->parameters = $parameters;
        $this->services = $services;

        $this->isBuilt = true;
    }

    public function get(string $name)
    {
        if (!array_key_exists($name, $this->services)) {
            throw new LogicException("Service '$name' not found in container");
        }

        return $this->services[$name];
    }

    public function getParameter(string $name)
    {
        if (!array_key_exists($name, $this->parameters)) {
            throw new LogicException("Parameter '$name' not found in container");
        }

        return $this->parameters[$name];
    }
}