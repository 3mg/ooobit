<?php


namespace App\Service;


use App\Exception\InvalidCredentialsException;
use App\Model\User;

class Security
{
    private UserManager   $userManager;
    private SessionHelper $sessionHelper;

    public function __construct(UserManager $userManager, SessionHelper $sessionHelper)
    {
        $this->userManager = $userManager;
        $this->sessionHelper = $sessionHelper;
    }

    public function getAuthenticatedUser(): ?User
    {
        $this->sessionHelper->startSession(
            true
        ); // No need to run $this->sessionHelper->writeAndClose because session is auto closing after the first read

        return isset($_SESSION["username"]) ? $this->userManager->findUserByUsername($_SESSION["username"]) : null;
    }

    public function getAuthenticatedUserName(): string
    {
        $this->sessionHelper->startSession(
            true
        ); // No need to run $this->sessionHelper->writeAndClose because session is auto closing after the first read

        return $_SESSION["username"] ?? '';
    }

    /**
     * @param string $username
     * @param string $password
     * @return User
     * @throws InvalidCredentialsException
     */
    public function authenticateUser(string $username, string $password): User
    {
        $user = $this->userManager->findUserByUsername($username);

        if (!$user || !password_verify($password, $user->passwordHash)) {
            throw new InvalidCredentialsException();
        }

        $this->sessionHelper->startSession();

        $_SESSION['username'] = $username;

        $this->sessionHelper->writeAndClose();

        return $user;
    }

    public function logoutUser(): void
    {
        $this->sessionHelper->startSession();

        if (isset($_SESSION['username'])) {
            unset($_SESSION['username']);
        }

        $this->sessionHelper->writeAndClose();
    }
}