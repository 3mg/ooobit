<?php


namespace App\Service;


class SessionHelper
{
    private int $sessionTtl;

    /**
     * SessionHelper constructor.
     * @param int $sessionTtl
     */
    public function __construct(int $sessionTtl)
    {
        $this->sessionTtl = $sessionTtl;
    }

    public function startSession(bool $readAndClose = false): void
    {
        session_start(
            [
                'cookie_lifetime' => $this->sessionTtl,
                'read_and_close'  => $readAndClose,
            ]
        );
    }

    public function writeAndClose(): void
    {
        session_write_close();
    }
}