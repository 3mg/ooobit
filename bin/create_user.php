<?php

if (count($argv) !== 4) {
    echo 'Usage: php bin/create_user.php username balance plainPassword'.PHP_EOL;
    exit;
}

require_once dirname(__DIR__).'/vendor/autoload.php';

use App\App;
use App\Model\User;
use App\Service\UserManager;

$app = new App('dev');
$app->start();

/** @var UserManager $userManager */
$userManager = $app->getContainer()->get(UserManager::class);

$user = new User();
$user->username = $argv[1];
$user->displayName = $argv[1];
$user->balance = $argv[2];
$user->plainPassword = $argv[3];

$userManager->saveUser($user);

echo 'Created user: ';
var_dump($user);

$app->terminate();
