<?php

require_once dirname(__DIR__).'/vendor/autoload.php';

use App\App;

$app = new App('dev');
$app->start();

/** @var PDO $pdo */
$pdo = $app->getContainer()->get(PDO::class);

$pdo->exec(
    'CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(255) NOT NULL, salt VARCHAR(255) NOT NULL, passwordHash VARCHAR(255) NOT NULL, displayName VARCHAR(255) DEFAULT NULL, balance NUMERIC(15, 2) NOT NULL, UNIQUE INDEX UNIQ_USENAME (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
);

echo 'Database initialized'.PHP_EOL;

$app->terminate();
