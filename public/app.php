<?php

require_once dirname(__DIR__).'/vendor/autoload.php';

use App\App;
use Jasny\HttpMessage\Emitter;
use Jasny\HttpMessage\ServerRequest;

$request = (new ServerRequest())->withGlobalEnvironment();

$app = new App('prod');
$app->start();

$response = $app->handleRequest($request);

$emitter = new Emitter();
$emitter->emit($response);

$app->terminate();
